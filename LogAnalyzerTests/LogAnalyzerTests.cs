﻿using NUnit.Framework;
using System;

namespace LogAnalyzerTests
{
	[TestFixture]
	public class LogAnalyzerTests
	{
		//run test, dependencies are not isolated
		//this test case will fail because of the dependency not being able to read the file
		[Test]
		public void TestValidNameWithDependency()
		{
			string testFile = "validname.txt";
			bool expected = true,
					actual;
			//Create a log analyzer using the FileExtensionManager implementation of IExtensionManager
			//FileExtensionManager reads from a file (FileExtensionManager.EXTENSIONS_FILE) to check for valid file types
			//The test fails as a result of the dependency, not the logic behind LogAnalyzer
			LogAnalyzer.LogAnalyzer logAnalyzer = new LogAnalyzer.LogAnalyzer ();

			actual = logAnalyzer.IsFilenameValid (testFile);
			Assert.AreEqual (expected, actual, "File name should have appropriate length and file extension.");
		}

		//run test, dependencies are isolated
		[Test]
		public void TestValidNameWithStub ()
		{
			string testFile = "validname.txt";
			bool expected = true,
					actual;
			//Create a log analyzer, pass in a stub extension manager that returns true when invoking IsExtensionValid,
			//allowing us to circumvent the dependency's logic - the dependency's logic is tested in its own dedicated test cases
			//As the test case supplies a name that is valid according to the specified name length requirements,
			//the only cause of failure for this test is when the input string is too short (intended behaviour)
			LogAnalyzer.LogAnalyzer logAnalyzer = new LogAnalyzer.LogAnalyzer (new StubExtensionManager(true));

			actual = logAnalyzer.IsFilenameValid (testFile);
			Assert.AreEqual (expected, actual, "File name should have appropriate length and file extension.");
		}
	}
}