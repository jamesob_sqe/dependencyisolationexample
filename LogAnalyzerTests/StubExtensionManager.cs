﻿using System;
using LogAnalyzer;

namespace LogAnalyzerTests
{
	public class StubExtensionManager : IExtensionManager
	{
		private bool returnValue;

		public StubExtensionManager (bool stubbedReturnValue)
		{
			returnValue = stubbedReturnValue;
		}

		public bool IsExtensionValid(string ext)
		{
			return returnValue;
		}
	}
}

