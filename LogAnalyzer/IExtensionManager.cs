﻿using System;

namespace LogAnalyzer
{
	public interface IExtensionManager
	{
		bool IsExtensionValid(string extension);
	}
}

