﻿using System;
using System.IO;
using System.Collections.Generic;

namespace LogAnalyzer
{
	public class FileExtensionManager : IExtensionManager
	{
		public const string EXTENSIONS_FILE = "extensions.txt";

		public FileExtensionManager ()
		{
		}

		public bool IsExtensionValid(string extension)
		{
			if (File.Exists (EXTENSIONS_FILE))
			{
				List<String> extensions = new List<String>(File.ReadLines(EXTENSIONS_FILE));
				return extensions.Contains (extension);
			}
			return false;
		}
	}
}

