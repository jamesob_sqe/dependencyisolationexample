﻿using System;

namespace LogAnalyzer
{
	public class LogAnalyzer
	{
		IExtensionManager extManager;

		public LogAnalyzer()
		{
			extManager = new FileExtensionManager ();
		}

		public LogAnalyzer (IExtensionManager manager)
		{
			extManager = manager;
		}

		/// <summary>
		/// Determines if the log file name has an appropriate length (at least 5 characters long, excluding extension) and a compatible file extension.
		/// </summary>
		/// <returns>returns <c>true</c> if the file name meets the specified length criteria, and has a file extension that is specified in the file containing compatible file extensions</returns>
		/// <param name="filename">The name of the file (including file extension) to be validated</param>
		/// <see cref="IExtensionManager"/>
		/// <see cref="FileExtensionManager"/>  
		public bool IsFilenameValid(string filename)
		{
			int lastPeriod = filename.LastIndexOf (".");
			string name = filename.Substring (0, lastPeriod);
			string ext = filename.Substring (lastPeriod);
			return name.Length > 5 && extManager.IsExtensionValid (ext);
		}
	}
}

